
public class Transaction {

	public enum TYPE{
		CREDIT, DEBIT
	}
	
	private TYPE type;
	private double value;
	private String reason;
	private Account account;
	
	public Transaction(TYPE _type, double _value, String _reason, Account _account) {
		this.type = _type;
		this.value = _value;
		this.reason = _reason;
		this.account = _account;
	}
	
	public TYPE getType() {
		return this.type;
	}
	
	public void setType(TYPE _type) {
		this.type = _type;
	}

	public double getValue() {
		return this.value;
	}
	
	public void setValue(double _value) {
		this.value = _value;
	}
	
	public String getReason() {
		return this.reason;
	}
	
	public void setReason(String _reason) {
		this.reason = _reason;
	}
	
	public Account getAccount() {
		return this.account;
	}
	
	public void setAccount(Account _account) {
		this.account = _account;
	}
}
