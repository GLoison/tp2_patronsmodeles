import java.util.ArrayList;
import java.util.List;

public class Bank {

	private List<Client> clients;
	
	public Bank() {
		this.clients = new ArrayList<Client>();
	}
	
	public void addClient(Client _client) {
		this.clients.add(_client);
	}
	
	public void removeClient(Client _client) {
		this.clients.remove(_client);
	}
}
