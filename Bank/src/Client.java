import java.util.ArrayList;
import java.util.List;

public class Client {

	private String name;
	private List<Account> accounts;
	
	public Client(String _name) {
		this.name = _name;
		this.accounts = new ArrayList<Account>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String _name) {
		this.name = _name;
	}
	
	public List<Account> getAccounts(){
		return this.accounts;
	}
	
	public void addAccount(Account _account) {
		this.accounts.add(_account);
	}
	
	public void removeAccount(Account _account) {
		this.accounts.remove(_account);
	}
}
