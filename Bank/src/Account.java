import java.util.ArrayList;
import java.util.List;

public class Account {

	private Client client;
	private double value;
	private List<Transaction> transactions;
	
	public Account(Client _client) {
		this.client = _client;
		this.value = 0;
		this.transactions = new ArrayList<Transaction>();
	}
	
	public Client getClient() {
		return this.client;
	}
	
	public void setClient(Client _client) {
		this.client = _client;
	}
	
	public double getValue() {
		return this.value;
	}
	
	public void setValue(double _value) {
		this.value = _value;
	}
	
	public void addValue(double _value) {
		setValue(this.value + _value);
	}
	
	public void substractValue(double _value) {
		setValue(this.value - _value);
	}
	
	public void addTransaction(Transaction t) {
		this.transactions.add(t);
		if(t.getType() == Transaction.TYPE.CREDIT) {
			addValue(t.getValue());
		}
		if(t.getType() == Transaction.TYPE.DEBIT) {
			substractValue(t.getValue());
		}
	}
}
