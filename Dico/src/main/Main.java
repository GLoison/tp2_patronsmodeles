package main;

import java.util.Scanner;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import english_dico.English_Dictionnary;
import french_dico.French_Dictionnary;
import similarity.WordSimilarity;
import similarity.WordSimilarity.Language;

public class Main implements BundleActivator{

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Main.context = bundleContext;
		
		French_Dictionnary frenchActivator = new French_Dictionnary();
		frenchActivator.start(bundleContext);
		English_Dictionnary englishActivator = new English_Dictionnary();
		englishActivator.start(bundleContext);
		WordSimilarity similarities = new WordSimilarity();
		similarities.start(bundleContext);
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("Ecrivez votre mot :");
			String str = sc.nextLine().toLowerCase();
			if(!frenchActivator.verify(str)) {
				similarities.verify(str, Language.FRANCAIS);
			}
			if(!englishActivator.verify(str)) {
				similarities.verify(str, Language.ENGLISH);
			}
		}
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Main.context = null;
	}

}
