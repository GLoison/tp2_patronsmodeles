package dictionnary;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Dictionnary {

	private List<String> wordList;
	
	public Dictionnary(InputStream is) throws IOException {
		this.wordList = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String s;
		while((s = br.readLine()) != null) {
			wordList.add(s.toLowerCase());
		}
	}
	
	public boolean checkWord(String s) {
		return this.wordList.contains(s);
	}
	
	public void print() {
		for(String s : wordList) {
			System.out.println(s);
		}
	}
}
