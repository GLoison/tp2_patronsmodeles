package similarity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import english_dico.English_Dictionnary;
import french_dico.French_Dictionnary;

public class WordSimilarity implements BundleActivator {

	private static BundleContext context;
	
	public enum Language{
		FRANCAIS, ENGLISH
	}

	static BundleContext getContext() {
		return context;
	}
	
	public void verify(String word, Language l) {
		if(l == Language.FRANCAIS) {
			boolean b = false;
			StringBuilder sb = new StringBuilder("Les mots suivants existent : ");
			BufferedReader br = new BufferedReader(new InputStreamReader(French_Dictionnary.class.getResourceAsStream("fran�ais.txt")));
			String s;
			try {
				while((s = br.readLine()) != null) {
					if (s.toLowerCase().startsWith(word)) {
						sb.append(s);
						sb.append(", ");
						b = true;
					}
				}
				sb.delete(sb.length()-2, sb.length()-1);
				if(b) {
					System.out.println(sb.toString());
				}
				else {
					System.out.println("Aucun mot n'a �t� trouv�");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(l == Language.ENGLISH) {
			boolean b = false;
			StringBuilder sb = new StringBuilder("Les mots suivants existent : ");
			BufferedReader br = new BufferedReader(new InputStreamReader(English_Dictionnary.class.getResourceAsStream("english.txt")));
			String s;
			try {
				while((s = br.readLine()) != null) {
					if (s.toLowerCase().startsWith(word)) {
						sb.append(s);
						sb.append(", ");
						b = true;
					}
				}
				sb.delete(sb.length()-2, sb.length()-1);
				if(b) {
					System.out.println(sb.toString());
				}
				else {
					System.out.println("No words has been found");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void start(BundleContext bundleContext) throws Exception {
		WordSimilarity.context = bundleContext;
	}

	public void stop(BundleContext bundleContext) throws Exception {
		WordSimilarity.context = null;
	}

}
