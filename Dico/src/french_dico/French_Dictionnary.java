package french_dico;

import java.util.Hashtable;
import java.util.Scanner;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import dictionnary.Dictionnary;

public class French_Dictionnary implements BundleActivator {

	private static BundleContext context;
	private Dictionnary dico;

	static BundleContext getContext() {
		return context;
	}
	
	public boolean verify(String word) {
		if(this.dico.checkWord(word)) {
			System.out.println("Le mot existe");
			return true;
		}
		else {
			System.out.println("Le mot n'existe pas");
			return false;
		}
	}

	public void start(BundleContext bundleContext) throws Exception {
		French_Dictionnary.context = bundleContext;
		this.dico = new Dictionnary(this.getClass().getResourceAsStream("fran�ais.txt"));
		Hashtable<String, String> proprietes = new Hashtable<String, String>();
		proprietes.put("langue", "Fran�ais");
		ServiceRegistration servReg = context.registerService(Dictionnary.class.getName(), this.dico, proprietes);
	}

	public void stop(BundleContext bundleContext) throws Exception {
		French_Dictionnary.context = null;
	}

}
