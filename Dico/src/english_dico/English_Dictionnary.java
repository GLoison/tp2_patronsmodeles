package english_dico;

import java.util.Hashtable;
import java.util.Scanner;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import dictionnary.Dictionnary;

public class English_Dictionnary implements BundleActivator {

	private static BundleContext context;
	private Dictionnary dico;

	static BundleContext getContext() {
		return context;
	}
	
	public boolean verify(String word) {
		if(this.dico.checkWord(word)) {
			System.out.println("The word exists");
			return true;
		}
		else {
			System.out.println("The word doesn't exist");
			return false;
		}
	}

	public void start(BundleContext bundleContext) throws Exception {
		English_Dictionnary.context = bundleContext;
		this.dico = new Dictionnary(this.getClass().getResourceAsStream("english.txt"));
		Hashtable<String, String> proprietes = new Hashtable<String, String>();
		proprietes.put("langue", "English");
		ServiceRegistration servReg = context.registerService(Dictionnary.class.getName(), this.dico, proprietes);
	}

	public void stop(BundleContext bundleContext) throws Exception {
		English_Dictionnary.context = null;
	}

}
